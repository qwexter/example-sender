from urllib.request import urlopen
from urllib.parse import urlencode
import os
import argparse
import sys


def prepare_app_url(artifact_name, from_job):
    return "https://gitlab.com/{project_info}/-/jobs/artifacts/{commit}/raw/${artifact}?job={job_name}".format(
        project_info=os.environ["CI_PROJECT_PATH"], commit=os.environ["CI_COMMIT_REF_NAME"], artifact=artifact_name, job_name=from_job)


def prepare_app_mesage(artifact_name, download_url):
    return "New build available!\nHere [{artifact_name}]({download_url})".format(artifact_name=artifact_name, download_url=download_url)


def send_message(message, to, token):
    send_url = "https://myteam.mail.ru/bot/v1/messages/sendText"
    params = {
        "token": token,
        "chatId": to,
        "text": message,
        "parseMode": "MarkdownV2"
    }
    encoded_params = urlencode(params)
    full_url = "?".join([send_url, encoded_params])
    with urlopen(full_url) as response:
        body = response.read().decode('utf-8')
        if 'true' not in body:
            raise Exception("Failed api call. Response: " + body)


if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(
            description="Prepare and send message with information about new builds")
        parser.add_argument(
            "artifactName", help="APK file name with extention (like something.apk)")
        parser.add_argument(
            "fromJob", help="What a job we using for artifacts storing")
        parser.add_argument("token", help="Bot's access token")
        parser.add_argument("to",  metavar='to', nargs="*",
                            help="Users or Group Ids where we want send messages")
        args = parser.parse_args()

        artifact_url = prepare_app_url(args.artifactName, args.fromJob)

        print(artifact_url)
        message = prepare_app_mesage(args.artifactName, "artifact_url")
        print(message)
        send_message(message, args.to, args.token)

    except Exception as err:
        print(err)
        sys.exit(2)
    else:
        print("Message sent to users")
